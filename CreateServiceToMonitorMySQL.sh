#!/bin/bash

sudo apt-get install mysql
sudo systemctl start mysql
sudo systemctl enable mysql

# Create a Variable
STATUS="$(systemctl is-active mysql.service)"

# Conditional Statements
if [ "$STATUS" == "inactive" ]; 
then
  echo "starting MySQL service"
  systemctl restart mysql.service
else
 echo "MySQL is currently running"
fi

# crontab -e 
*/5 * * * * bash /home/ubuntu/mysql-monitor.sh >> /var/log/mysql-monitor.log

