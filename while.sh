echo "Enter a five-digit ZIP code: "
read ZIP

while echo $ZIP | egrep -v "^[0-9]{5}$" 
do 
  echo "you must enter a valid ZIP code - five digits only!"
  echo "Enter a five-digit ZIP code: "
  read ZIP
done 

echo "Thankyou!"
