#!/bin/bash

# Setup Hostname 
sudo hostnamectl set-hostname "web.softobiz.com"

# Update the hostname part of Host File
sudo echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 

# Install Webserver 
sudo apt install apache2 -y 

sudo echo "Welcome to SoftoBiz DevOps Team" > /var/www/html/index.html

# Download, Install & Configure Web Server i.e. Apache2 
sudo apt install mysql-server -y 


