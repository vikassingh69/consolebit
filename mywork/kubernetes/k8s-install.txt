#!/bin/bash

# Setup Hostname 
hostnamectl set-hostname "k8s-controller.softobiz.com"

# Configure Hostname unto hosts file 
echo "`hostname -I | awk '{ print $1}'` `hostname`" >> /etc/hosts 

# Refresh the Terminal 
/bin/bash 

# Update the Ubuntu Local Repository with Online Repository 
sudo apt update 

# Download, Install & Configure Utility Softwares 
sudo apt install git curl unizp tree wget -y 

# Install Containerization Technology 
sudo apt install docker.io -y 

# Enable Docker For Ubuntu User 
sudo usermod -aG docker ubuntu 

# Create a File and Update 
sudo touch /etc/docker/daemon.json

# docker change cgroup driver to systemd
echo '{'  >> /etc/docker/daemon.json
echo '"exec-opts": ["native.cgroupdriver=systemd"]'  >> /etc/docker/daemon.json
echo '}'  >> /etc/docker/daemon.json

# Enable Docker Services at boot level
sudo systemctl enable docker
sudo systemctl restart docker

# Update the apt package index and install packages needed to use the Kubernetes apt repository:
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates

# Download the Google Cloud public signing key:
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# Add the Kubernetes apt repository:
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Update apt package index, install kubelet, kubeadm and kubectl, and pin their version:
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl



kubeadm join 172.31.35.151:6443 --token 7rphw3.gloex2bmgzgguey3 \
        --discovery-token-ca-cert-hash sha256:a8d559b4c81f647478c91e3fb6ea4045993cf8fb8ae148e13e713d1f7f5c3c46



The job identifier is 418508.
Feb 04 02:41:37 node1-k8s.softobiz.com kubelet[39402]: E0204 02:41:37.263724   39402 server.go:205] "Failed to load kubelet config file" err="failed>Feb 04 02:41:37 node1-k8s.softobiz.com systemd[1]: kubelet.service: Main process exited, code=exited, status=1/FAILURE
-- Subject: Unit process exited
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
--
-- An ExecStart= process belonging to unit kubelet.service has exited.
--
-- The process' exit code is 'exited' and its exit status is 1.
Feb 04 02:41:37 node1-k8s.softobiz.com systemd[1]: kubelet.service: Failed with result 'exit-code'.
-- Subject: Unit failed
-- Defined-By: systemd


Environment="KUBELET_SYSTEM_PODS_ARGS=--pod-manifest-path=/etc/kubernetes/manifests --allow-privileged=true --fail-swap-on=false"

echo 'Environment="KUBELET_EXTRA_ARGS=--fail-swap-on=false"' > /etc/systemd/system/kubelet.service.d/90-local-extras.conf