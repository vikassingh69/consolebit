#!/bin/bash 

aws ec2 run-instances \
--image-id "ami-0af25d0df86db00c1" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-0e3881d6ee3f69f64" \
--security-group-ids "sg-028eb957b05a27cf7" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Nginx_Web_Server}]' \
--key-name "softodevops" \
--user-data file://web-server-nginx.txt --profile vikas_singh
