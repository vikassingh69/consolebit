# Download, Install Java 11
sudo apt-get install openjdk-11-jdk -y

# Backup the Environment File
sudo cp -pvr /etc/environment "/etc/environment_$(date +%F_%R)"

# Create Environment Variables 
echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" >> /etc/environment

# Compile the Configuration 
source /etc/environment

# Update the Repository on Ubuntu 18.04
sudo apt-get update 

# Download, Install Jenkins 
sudo apt-get install jenkins -y

# Verify the jenkins service 
sudo systemctl status jenkins.service

# Enable Jenkins Daemon/Service at Boot
sudo systemctl enable jenkins.service

# Restart the Jenkins Daemon/Service 
sudo systemctl restart jenkins.service

# Usig Process Status Command 
ps -aux | grep jenkins 