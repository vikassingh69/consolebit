#!/bin/bash

# Setup New Hostname
hostnamectl set-hostname "apache-httpd.softobiz.com"

# Configure New Hostname as part of /etc/hosts file 
echo "`hostname -I | awk '{ print $1}'` `hostname`" >> /etc/hosts

# Refresh the Terminal
/bin/bash 

apt update

apt install git wget unzip curl tree -y 

apt install apache2 -y

cd /opt/

git clone https://github.com/Alphanum404/personal-web.git

cd personal-web

mv * /var/www/html/
 
sudo systemctl restart apache2.service

cd ..

apt install mysql* -y

sudo apt-get install openjdk-11-jdk -y

echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" >> /etc/environment

source /etc/environment  #compile env

#tomcat

#sudo wget https://downloads.apache.org/tomcat/tomcat-8/v8.5.75/bin/apache-tomcat-8.5.75.tar.gz

#sudo tar xvzf apache-tomcat-8.5.75.tar.gz

#sudo mv apache-tomcat-8.5.75.tar.gz tomcat

#cd /

#cd /opt/tomcat/bin/

#./startup.sh
