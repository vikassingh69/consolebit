#!/bin/bash

# Setup Hostname 
#sudo hostnamectl set-hostname "db.vikas@softobiz.com"

# Update the hostname part of Host File
#echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts 

# Update Ubuntu Repository 
#apt update 

# Download, & Install Utility Softwares 
sudo apt install git wget unzip curl tree -y 

# Download, Install & Configure Data Server i.e mysql
sudo apt install mysql-server -y 


