#!/bin/bash 

# jenkins-ansible Of Linux - Ubuntu 20.04
aws ec2 run-instances \
--image-id "ami-0851b76e8b1bce90b" \
--instance-type "t2.micro" \
--count 1 \
--subnet-id "subnet-0e3881d6ee3f69f64" \
--security-group-ids "sg-028eb957b05a27cf7" \
--tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=jenkins-server},{Key=Environment,Value=Development},{Key=ProjectName,Value=SoftoBizDevOps},{Key=ProjectID,Value=20220114},{Key=EmailID,Value=vikas@softobiz.com}]' \
--key-name "softodevops" \
--user-data file://jenkins-hostname.txt --profile vikas_singh 